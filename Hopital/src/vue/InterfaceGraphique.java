/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javax.swing.*;
import java.awt.*;

/**
 * @author Emma
 */
public class InterfaceGraphique extends JFrame {
    
    /**
     * On instancie un objet JPanel
     */
    private final JPanel pan = new JPanel();
    
    public InterfaceGraphique(){ // constructeur 
        /**
         * On définit le titre de notre fenêtre
         */
        this.setTitle("Gestion du centre hospitalier - Menu principal");
        /**
         * On définit la taille de notre fenêtre
         */
        this.setSize(600, 600);
        /**
         * On définit la position initiale de la fenêtre
         */
        this.setLocationRelativeTo(null);
        /**
         * On ferme arrete le programme lorsqu'on ferme la fenêtre
         */
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
         /**
         * On ajoute le panneau dans la fenêtre
         */
        this.getContentPane().add(pan);
        this.setContentPane(new Panneau());

        
        this.setVisible(true); 
        
     }
    

    
}


